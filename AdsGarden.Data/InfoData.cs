﻿namespace AdsGarden.Data
{
    using AdsGarden.Data.Core;

    public class InfoData : Core.EntityBaseData<Model.Info>
    {
        public InfoData() : base(new DataContext())
        {

        }
    }
}
