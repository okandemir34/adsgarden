﻿namespace AdsGarden.Data.Core
{
    using AdsGarden.Model;
    using System.Data.Entity;

    public class DataContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Content> Contents { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<Info> Infos { get; set; }
        public DbSet<Page> Pages { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().ToTable("ads_User");
            modelBuilder.Entity<Content>().ToTable("ads_Content");
            modelBuilder.Entity<Category>().ToTable("ads_Category");
            modelBuilder.Entity<Setting>().ToTable("ads_Setting");
            modelBuilder.Entity<Info>().ToTable("ads_Infos");
            modelBuilder.Entity<Page>().ToTable("ads_Pages");

            base.OnModelCreating(modelBuilder);
        }
    }
}