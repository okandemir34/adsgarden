﻿namespace AdsGarden.Data
{
    using AdsGarden.Data.Core;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class PageData : Core.EntityBaseData<Model.Page>
    {
        public PageData() : base(new DataContext())
        {

        }
    }
}
