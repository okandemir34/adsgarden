﻿namespace AdsGarden.Data
{
    using AdsGarden.Data.Core;

    public class CategoryData : Core.EntityBaseData<Model.Category>
    {
        public CategoryData() : base(new DataContext())
        {

        }
    }
}
