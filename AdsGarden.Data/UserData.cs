﻿namespace AdsGarden.Data
{
    using AdsGarden.Data.Core;

    public class UserData : Core.EntityBaseData<Model.User>
    {
        public UserData() : base(new DataContext())
        {

        }
    }
}
