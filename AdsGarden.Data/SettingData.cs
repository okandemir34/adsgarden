﻿namespace AdsGarden.Data
{
    using AdsGarden.Data.Core;

    public class SettingData : Core.EntityBaseData<Model.Setting>
    {
        public SettingData() : base(new DataContext())
        {

        }
    }
}
