﻿namespace AdsGarden.YouVolume.Controllers
{
    using AdsGarden.Data;
    using AdsGarden.YouVolume.Helpers;
    using AdsGarden.YouVolume.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    public class HomeController : Controller
    {
        ContentData _contentData;

        public HomeController()
        {
            _contentData = new ContentData();
        }

        public ActionResult Index()
        {
            var contents = _contentData.GetBy(x => x.IsActive && !x.IsDeleted && x.PublishDate <= DateTime.Now, "PublishDate", true).Take(10).ToList();

            var model = new HomeViewModel()
            {
                Contents = contents,
            };

            return View(model);
        }

        public ActionResult ClearCache()
        {
            CacheHelper.ClearCaches();
            return RedirectToAction("Index","Home", new { q = "clear-cache" });
        }
    }
}