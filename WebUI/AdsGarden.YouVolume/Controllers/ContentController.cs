﻿namespace AdsGarden.YouVolume.Controllers
{
    using AdsGarden.Data;
    using AdsGarden.YouVolume.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web;
    using System.Web.Mvc;

    public class ContentController : Controller
    {
        ContentData _contentData;
        InfoData _infoData;

        public ContentController()
        {
            _contentData = new ContentData();
            _infoData = new InfoData();
        }

        public ActionResult Index(string slug)
        {
            if (string.IsNullOrEmpty(slug))
                return RedirectToAction("Index", "Home", new { t = "no-slug" });

            var content = _contentData.GetBy(x => x.Slug == slug && x.IsActive).LastOrDefault();

            if (content == null)
                return View("Index", "Home", new { e = "not-found-content" });

            var info = _infoData.GetBy(x => x.ContentId == content.Id).FirstOrDefault();

            var related = _contentData.GetRandom(x => x.CategoryId == content.CategoryId && x.SiteId == content.SiteId && x.PublishDate <= DateTime.Now, 3);

            var model = new ContentViewModel
            {
                Content = content,
                RelatedContent = related,
                Info = info
            };

            return View(model);
        }

        public ActionResult Amp(string slug)
        {
            if (string.IsNullOrEmpty(slug))
                return RedirectToAction("Index", "Home", new { t = "no-slug" });

            var content = _contentData.GetBy(x=>x.Slug == slug && x.IsActive).LastOrDefault();

            if (content == null)
                return View("Index", "Home", new { e = "not-found-content" });

            var related = _contentData.GetRandom(x => x.CategoryId == content.CategoryId && x.SiteId == content.SiteId && x.PublishDate <= DateTime.Now, 4);

            var model = new ContentViewModel
            {
                Content = content,
                RelatedContent = related,
            };

            return View(model);
        }
    }
}