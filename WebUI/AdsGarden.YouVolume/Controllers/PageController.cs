﻿namespace AdsGarden.YouVolume.Controllers
{
    using AdsGarden.Data;
    using AdsGarden.YouVolume.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    public class PageController : Controller
    {
        PageData _pageData;

        public PageController()
        {
            _pageData = new PageData();
        }

        public ActionResult Index(string slug)
        {
            if(string.IsNullOrEmpty(slug))
                return RedirectToAction("Index", "Home", new { q = "page-no-slug" });

            var page = _pageData.GetBy(x => x.Slug == slug).FirstOrDefault();
            if(page == null)
                return RedirectToAction("Index", "Home", new { q = "page-no-slug" });

            return View(page);
        }
    }
}