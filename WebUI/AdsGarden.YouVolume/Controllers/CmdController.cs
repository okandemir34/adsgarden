﻿

namespace AdsGarden.YouVolume.Controllers
{
    using AdsGarden.Data;
    using AdsGarden.YouVolume.Models;
    using AdsGarden.YouVolume.Helpers;
    using System.Web.Mvc;
    using AdsGarden.Model;
    using System.Collections.Generic;
    using System.Linq;
    using System;
    using AdsGarden.Helpers;

    public class CmdController : Controller
    {
        public Model.Setting Setting = CacheHelper.Settings;

        ContentData _contentData;
        SettingData _settingData;
        InfoData _infoData;

        public CmdController()
        {
            _contentData = new ContentData();
            _settingData = new SettingData();
            _infoData = new InfoData();
        }

        [Authentication]
        [HttpGet]
        public ActionResult Index(int page = 1)
        {
            var list = _contentData.GetBy(x => x.SiteId == CacheHelper.Settings.Id && x.IsActive && x.PublishDate > DateTime.Now).Take(5).ToList();
            var count = _contentData.GetCount(x => x.SiteId == CacheHelper.Settings.Id && x.IsActive);
            var wait = _contentData.GetBy(x => x.SiteId == CacheHelper.Settings.Id && x.IsActive && x.PublishDate < DateTime.Now).Take(5).ToList();

            var model = new AdminIndexViewModel()
            {
                Contents = list,
                ContentCount = count,
                PublishContents = wait,
            };

            return View(model);
        }

        [Authentication]
        [HttpGet]
        public ActionResult Content(int page = 1)
        {
            var list = _contentData.GetByPage(x=>x.IsActive && !x.IsDeleted,page, 20, "Id", true);
            var count = _contentData.GetCount();

            var model = new ContentCmdListViewModel()
            {
                Count = count,
                CurrentPage = page,
                Contents = list
            };

            return View(model);
        }

        [Authentication]
        [HttpPost]
        public ActionResult Content(string username)
        {
            if (username.Length <= 3)
            {
                ViewBag.Message = "Arama Kelimesi en az 4 karakter olmlıdır";
                return View(new ContentCmdListViewModel()
                {
                    Count = 0,
                    CurrentPage = 1,
                    Contents = new List<Content>()
                });
            }

            var list = _contentData.GetBy(x => x.Title.Contains(username));
            ViewBag.Search = username;

            return View(new ContentCmdListViewModel()
            {
                Count = list.Count,
                CurrentPage = 1,
                Contents = list
            });
        }

        [Authentication]
        [HttpGet]
        public ActionResult ContentEdit(int id)
        {
            var p = _contentData.GetByKey(id);
            var info = _infoData.GetBy(x => x.ContentId == id).FirstOrDefault();
            var model = new ContentAddViewModel()
            {
                Content = p,
                Info = info,
            };

            return View(model);
        }

        [Authentication]
        [HttpPost, ValidateInput(false)]
        public ActionResult ContentEdit(ContentAddViewModel x, string imageFile)
        {
            var validationErrors = new List<string>();
            if (string.IsNullOrEmpty(x.Content.Title))
                validationErrors.Add("Başlık alanı zorunludur");
            if (string.IsNullOrEmpty(x.Content.MetaTitle))
                validationErrors.Add("Meta Başlık alanı zorunludur");
            if (string.IsNullOrEmpty(x.Content.MetaDescription))
                validationErrors.Add("Meta Açıklama alanı zorunludur");
            if (string.IsNullOrEmpty(x.Content.Keywords))
                x.Content.Keywords = "";
            if (x.Content.PublishDate.Year == 1)
                validationErrors.Add("Tarih Sıkıntılı kanka");
            if (string.IsNullOrEmpty(x.Content.Description))
                validationErrors.Add("Açıklama alanı zorunludur");

            var profileInDb = _contentData.GetByKey(x.Content.Id);

            if (!string.IsNullOrEmpty(imageFile))
            {
                try
                {
                    var uploadResult = CdnHelper.UploadBase64(imageFile, x.Content.Title);

                    if (string.IsNullOrEmpty(uploadResult))
                        throw new Exception("Cdn'e Resim yuklenemedi!");

                    if (System.IO.File.Exists(profileInDb.ImagePath))
                        System.IO.File.Delete(profileInDb.ImagePath);

                    profileInDb.ImagePath = uploadResult;
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Resim Yüklenirken Hata Oldu...";
                    return View(x);
                }
            }

            if (validationErrors.Count > 0)
            {
                var viewModelResult = new ViewModelResult(false, "Doğrulama Hataları", validationErrors);
                ViewBag.ViewModelResult = viewModelResult;
                return View(x);
            }

            profileInDb.Title = x.Content.Title;
            profileInDb.Description = x.Content.Description;
            profileInDb.Keywords = x.Content.Keywords;
            profileInDb.MetaDescription = x.Content.MetaDescription;
            profileInDb.MetaTitle = x.Content.MetaTitle;
            profileInDb.UpdateDate = DateTime.Now;
            

            var result = _contentData.Update(profileInDb);
            if(result.IsSucceed)
            {
                var infoDb = _infoData.GetBy(y => y.ContentId == x.Content.Id).FirstOrDefault();
                infoDb.Climate = x.Info.Climate;
                infoDb.Desinger = x.Info.Desinger;
                infoDb.GardenStyle = x.Info.GardenStyle;
                infoDb.Location = x.Info.Location;
                infoDb.Owner = x.Info.Owner;
                infoDb.Size = x.Info.Size;

                var up = _infoData.Update(infoDb);

                ViewBag.ViewModelResult = new ViewModelResult(
                result.IsSucceed,
                result.IsSucceed ? "İşlem Tamamlandı. Makale Güncellendi." : "Hata Oluştu",
                result.Message);

                TempData["ViewModelResult"] = ViewBag.ViewModelResult;

                if (result.IsSucceed)
                    return RedirectToAction("Content", "Cmd");
            }

            return RedirectToAction("Content");
        }

        [Authentication]
        [HttpGet]
        public ActionResult ContentAdd()
        {
            var model = new ContentAddViewModel()
            {
                Content = new Model.Content(),
                Info = new Info(),
            };

            return View(model);
        }

        [Authentication]
        [HttpPost, ValidateInput(false)]
        public ActionResult ContentAdd(ContentAddViewModel x, string imageFile)
        {
            var validationErrors = new List<string>();
            if (string.IsNullOrEmpty(x.Content.Title))
                validationErrors.Add("Başlık alanı zorunludur");
            if (string.IsNullOrEmpty(x.Content.MetaTitle))
                validationErrors.Add("Meta Başlık alanı zorunludur");
            if (string.IsNullOrEmpty(x.Content.MetaDescription))
                validationErrors.Add("Meta Açıklama alanı zorunludur");
            if (string.IsNullOrEmpty(x.Content.Keywords))
                x.Content.Keywords = "";
            if (x.Content.PublishDate.Year == 1)
                validationErrors.Add("Tarih Sıkıntılı kanka");
            if (string.IsNullOrEmpty(x.Content.Description))
                validationErrors.Add("Açıklama alanı zorunludur");

            var slug = "";
            var checkContent = new Model.Content();
            if (!string.IsNullOrEmpty(x.Content.Title))
            {
                slug = Sluger.Seo.convert(x.Content.Title);
                checkContent = _contentData.GetBy(y => y.Slug == slug && x.Content.IsActive).FirstOrDefault();
                if (checkContent != null)
                    validationErrors.Add("Bu başlıkla daha önce kayıt eklemişsin kanka");
            }

            if (validationErrors.Count > 0)
            {
                var viewModelResult = new ViewModelResult(false, "Doğrulama Hataları", validationErrors);
                ViewBag.ViewModelResult = viewModelResult;
                return View(x);
            }
            
            if (!string.IsNullOrEmpty(imageFile))
            {
                try
                {
                    var uploadResult = CdnHelper.UploadBase64(imageFile, x.Content.Title);

                    if (string.IsNullOrEmpty(uploadResult))
                        throw new Exception("Cdn'e Resim yuklenemedi!");

                    x.Content.ImagePath = uploadResult;
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Resim Yüklenirken Hata Oldu...";
                    return View(x);
                }
            }
            else
                x.Content.ImagePath = "_assets/img/no-image.jpg";
            
            Random rnd = new Random();

            x.Content.CreateDate = DateTime.Now;
            x.Content.UpdateDate = DateTime.Now;
            x.Content.CategoryId = 1;
            x.Content.UserId = 1;
            x.Content.Slug = slug;
            x.Content.SiteId = CacheHelper.Settings.Id;

            var result = _contentData.Insert(x.Content);
            if(result.IsSucceed)
            {
                checkContent = _contentData.GetBy(y => y.Slug == slug && x.Content.IsActive).FirstOrDefault();
                if (checkContent != null)
                {
                    x.Info.ContentId = checkContent.Id;
                    var up = _infoData.Insert(x.Info);
                }
            }

            ViewBag.ViewModelResult = new ViewModelResult(
                result.IsSucceed,
                result.IsSucceed ? "İşlem Tamamlandı. Yeni Bir Makale Eklendi." : "Hata Oluştu",
                result.Message);

            TempData["ViewModelResult"] = ViewBag.ViewModelResult;

            if (result.IsSucceed)
                return RedirectToAction("Content", "Cmd");

            return View(x);
        }

        [Authentication]
        [HttpGet]
        public ActionResult DeleteContent(int id, int page = 1, string returnAction = "Content")
        {
            var model = _contentData.GetByKey(id);
            model.IsActive = false;
            model.IsDeleted = true;
            var update = _contentData.Update(model);
            return RedirectToAction(returnAction, new { page = page });
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string UserName, string Password)
        {
            if (UserName.Equals("manager") && Password.Equals("123456"))
            {
                SessionHelper.IsLoggedIn = true;
                return RedirectToAction("Content", "Cmd");
            }

            return View();
        }

        [Authentication]
        [HttpGet]
        public ActionResult Settings()
        {
            var model = _settingData.GetByKey(Setting.Id);
            return View(model);
        }

        [Authentication]
        [HttpPost, ValidateInput(false)]
        public ActionResult Settings(Setting model, string imageFile)
        {
            var validationErrors = new List<string>();
            if (string.IsNullOrEmpty(model.Ads300))
                model.Ads300 = "";
            if (string.IsNullOrEmpty(model.Ads600))
                model.Ads600 = "";
            if (string.IsNullOrEmpty(model.Analytics))
                validationErrors.Add("Analytics Kodu Ekle Bari Kanka");
            if (string.IsNullOrEmpty(model.Facebook))
                model.Facebook = "";
            if (string.IsNullOrEmpty(model.Instagram))
                model.Instagram = "";
            if (string.IsNullOrEmpty(model.LogoPath))
                model.LogoPath = "";
            if (string.IsNullOrEmpty(model.Pinterest))
                model.Pinterest = "";
            if (string.IsNullOrEmpty(model.SiteAdress))
                model.SiteAdress = "";
            if (string.IsNullOrEmpty(model.Sitename))
                model.Sitename = "";
            if (string.IsNullOrEmpty(model.Twitter))
                model.Twitter = "";
            if (string.IsNullOrEmpty(model.Youtube))
                model.Youtube = "";
            if (string.IsNullOrEmpty(model.AdsenseVerifiedCode))
                model.AdsenseVerifiedCode = "";

            if (validationErrors.Count > 0)
            {
                var viewModelResult = new ViewModelResult(false, "Doğrulama Hataları", validationErrors);
                ViewBag.ViewModelResult = viewModelResult;
                return View(model);
            }

            var dbInModel = _settingData.GetByKey(model.Id);

            if (!string.IsNullOrEmpty(imageFile))
            {
                try
                {
                    var uploadResult = CdnHelper.UploadBase64(imageFile, model.Sitename+"-logo", true);

                    if (string.IsNullOrEmpty(uploadResult))
                        throw new Exception("Cdn'e Resim yuklenemedi!");

                    dbInModel.LogoPath = uploadResult;
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Resim Yüklenirken Hata Oldu...";
                    return View(model);
                }
            }

            dbInModel.Ads300 = model.Ads300;
            dbInModel.Ads600 = model.Ads600;
            dbInModel.Analytics = model.Analytics;
            dbInModel.Facebook = model.Facebook;
            dbInModel.Instagram = model.Instagram;
            dbInModel.Pinterest = model.Pinterest;
            dbInModel.SiteAdress = model.SiteAdress;
            dbInModel.Sitename = model.Sitename;
            dbInModel.Twitter = model.Twitter;
            dbInModel.Youtube = model.Youtube;
            dbInModel.AdsenseVerifiedCode = model.AdsenseVerifiedCode;

            var update = _settingData.Update(dbInModel);

            if (update.IsSucceed)
            {
                var viewModelResult = new ViewModelResult(true, "Güncellendi", validationErrors);
                ViewBag.ViewModelResult = viewModelResult;
                CacheHelper.ClearCaches();
                return View(model);
            }

            return View(model);
        }
    }
}