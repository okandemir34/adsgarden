﻿namespace AdsGarden.YouVolume.Models
{
    using AdsGarden.Model;
    using System.Collections.Generic;

    public class CategoryDetailViewModel
    {
        public CategoryDetailViewModel()
        {
            Contents = new List<Content>();
            Category = new Category();
        }

        public Category Category { get; set; }
        public List<Content> Contents { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPageCount { get; set; }
    }
}