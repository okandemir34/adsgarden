﻿namespace AdsGarden.YouVolume.Models
{
    using AdsGarden.Model;
    using System.Collections.Generic;

    public class CategoryViewModel
    {
        public CategoryViewModel()
        {
            CategoryList = new List<Category>();
        }

        public List<Category> CategoryList { get; set; }
    }
}