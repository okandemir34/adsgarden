﻿namespace AdsGarden.YouVolume.Models
{
    using AdsGarden.Model;
    using System.Collections.Generic;

    public class ContentViewModel
    {
        public ContentViewModel()
        {
            RelatedContent = new List<Content>();
        }

        public List<Content> RelatedContent { get; set; }
        public Info Info { get; set; }
        public Content Content { get; set; }
    }
}