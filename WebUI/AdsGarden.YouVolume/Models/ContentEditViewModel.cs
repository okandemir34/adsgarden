﻿namespace AdsGarden.YouVolume.Models
{
    using AdsGarden.Model;
    using System.Collections.Generic;

    public class ContentEditViewModel
    {
        public Content Content { get; set; }
        public Info Info { get; set; }
        public int Page { get; set; }
    }
}