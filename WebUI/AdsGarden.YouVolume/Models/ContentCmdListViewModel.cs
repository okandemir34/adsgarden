﻿namespace AdsGarden.YouVolume.Models
{
    using AdsGarden.Model;
    using System.Collections.Generic;

    public class ContentCmdListViewModel
    {
        public List<Content> Contents { get; set; }
        public int Count { get; set; }
        public int CurrentPage { get; set; }
    }
}