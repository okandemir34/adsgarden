﻿using AdsGarden.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdsGarden.YouVolume.Models
{
    public class HomeViewModel
    {
        public HomeViewModel()
        {
            Contents = new List<Content>();
        }

        public List<Content> Contents { get; set; }
    }
}