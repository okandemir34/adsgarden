﻿using AdsGarden.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdsGarden.YouVolume.Models
{
    public class AdminIndexViewModel
    {
        public AdminIndexViewModel()
        {
            Contents = new List<Content>();
            PublishContents = new List<Content>();
        }

        public List<Content> Contents { get; set; }
        public int ContentCount { get; set; }
        public List<Content> PublishContents { get; set; }
    }
}