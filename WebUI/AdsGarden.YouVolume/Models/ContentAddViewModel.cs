﻿namespace AdsGarden.YouVolume.Models
{
    using AdsGarden.Model;
    using System.Collections.Generic;

    public class ContentAddViewModel
    {
        public Content Content { get; set; }
        public Info Info { get; set; }
    }
}