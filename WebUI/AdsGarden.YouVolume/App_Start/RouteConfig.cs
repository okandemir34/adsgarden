﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace AdsGarden.YouVolume
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "CatDetail",
                url: "category/{slug}/{page}",
                defaults: new { controller = "Category", action = "Index", page = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Page",
                url: "page/{slug}/",
                defaults: new { controller = "Page", action = "Index" }
            );

            routes.MapRoute(
                name: "Content",
                url: "{slug}",
                defaults: new { controller = "Content", action = "Index" }
            );

            routes.MapRoute(
                name: "AmpContent",
                url: "amp/{slug}",
                defaults: new { controller = "Content", action = "Amp" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
