﻿namespace AdsGarden.YouVolume.Helpers
{
    using Helpers;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;

    public class AuthenticationAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            bool isLogin = SessionHelper.IsLoggedIn;

            if (!HttpContext.Current.Request.IsLocal && !isLogin)
            {
                filterContext.Result = new RedirectToRouteResult(new
                    RouteValueDictionary(new { controller = "Cmd", action = "Login" }));
            }
        }
    }
}