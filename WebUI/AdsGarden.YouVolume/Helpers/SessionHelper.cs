﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace AdsGarden.YouVolume.Helpers
{
    public static class SessionHelper
    {
        private static string CurrentTag_Key = "CurrentTag";
        public static string CurrentTag
        {
            get {
                return HttpContext.Current.Session[CurrentTag_Key] == null
                    ? ""
                    : HttpContext.Current.Session[CurrentTag_Key].ToString();
            }
            set
            {
                HttpContext.Current.Session[CurrentTag_Key] = value;
            }
        }

        private static string IsLogin_SessionKey = "IsLogin_SessionKey";
        public static bool IsLoggedIn
        {
            get
            {
                if (HttpContext.Current.Session[IsLogin_SessionKey] == null)
                    HttpContext.Current.Session[IsLogin_SessionKey] = false;

                var isLogin = false;
                bool.TryParse(HttpContext.Current.Session[IsLogin_SessionKey].ToString(), out isLogin);

                return isLogin;
            }
            set
            {
                HttpContext.Current.Session[IsLogin_SessionKey] = value;
            }
        }
    }
}