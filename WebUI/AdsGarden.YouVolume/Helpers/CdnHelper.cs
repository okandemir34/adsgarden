﻿namespace AdsGarden.Helpers
{
    using System;
    using System.IO;
    using System.Web;

    public static class CdnHelper
    {
        public static string UploadBase64(string base64, string title, bool isLogo = false)
        {
            string[] extensions = base64.Split('/');
            extensions = extensions[1].Split(';');
            var extension = extensions[0];

            var guid = Guid.NewGuid().ToString();
            string[] baseSplit = base64.Split(',');
            var bytes = Convert.FromBase64String(baseSplit[1]);

            title = Sluger.Seo.convert(title);
            var filePath = "";
            filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/_uploads/content"), title + "." + extension);

            if (isLogo)
                filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/_uploads/logo"), title + "." + extension);
            
            using (var imageFile = new FileStream(filePath, FileMode.Create))
            {
                imageFile.Write(bytes, 0, bytes.Length);
                imageFile.Flush();
            }

            var xpath = "/_uploads/content/" + title + "." + extension;
            if (isLogo)
                xpath = "/_uploads/logo/" + title + "." + extension;
            
            return xpath;
        }
    }
}