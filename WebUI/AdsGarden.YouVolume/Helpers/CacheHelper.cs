﻿namespace AdsGarden.YouVolume.Helpers
{
    using AdsGarden.Data;
    using AdsGarden.Model;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Web;
    using System.Web.Caching;

    public static class CacheHelper
    {
        public static List<Category> Categories
        {
            get
            {
                if (HttpContext.Current.Cache["Categories"] == null)
                {
                    string siteId = ConfigurationManager.AppSettings["AdsGarden.SiteId"];
                    int sit = Int32.Parse(siteId);
                    var category = new CategoryData().GetBy(x=>x.SiteId == sit);

                    HttpContext.Current.Cache.Add(
                       "Categories"
                     , category
                     , null
                     , DateTime.Now.AddDays(1)
                     , Cache.NoSlidingExpiration
                     , CacheItemPriority.AboveNormal
                     , null);
                }

                return HttpContext.Current.Cache["Categories"] as List<Category>;
            }
        }

        public static List<Page> Pages
        {
            get
            {
                if (HttpContext.Current.Cache["Pages"] == null)
                {
                    string siteId = ConfigurationManager.AppSettings["AdsGarden.SiteId"];
                    int sit = Int32.Parse(siteId);
                    var pages = new PageData().GetBy(x => x.SiteId == sit);

                    HttpContext.Current.Cache.Add(
                       "Pages"
                     , pages
                     , null
                     , DateTime.Now.AddDays(1)
                     , Cache.NoSlidingExpiration
                     , CacheItemPriority.AboveNormal
                     , null);
                }

                return HttpContext.Current.Cache["Pages"] as List<Page>;
            }
        }

        public static List<Content> EditorPick
        {
            get
            {
                if (HttpContext.Current.Cache["EditorPick"] == null)
                {
                    string siteId = ConfigurationManager.AppSettings["AdsGarden.SiteId"];
                    int sit = Int32.Parse(siteId);
                    var category = new ContentData().GetRandom(x => x.IsActive && x.SiteId == sit && x.PublishDate <= DateTime.Now, 5);

                    HttpContext.Current.Cache.Add(
                       "EditorPick"
                     , category
                     , null
                     , DateTime.Now.AddDays(1)
                     , Cache.NoSlidingExpiration
                     , CacheItemPriority.AboveNormal
                     , null);
                }

                return HttpContext.Current.Cache["EditorPick"] as List<Content>;
            }
        }

        public static Setting Settings
        {
            get
            {
                if (HttpContext.Current.Cache["Setting"] == null)
                {
                    string siteId = ConfigurationManager.AppSettings["AdsGarden.SiteId"];
                    int sit = Int32.Parse(siteId);
                    var setting = new SettingData().GetByKey(sit);

                    HttpContext.Current.Cache.Add(
                       "Setting"
                     , setting
                     , null
                     , DateTime.Now.AddDays(1)
                     , Cache.NoSlidingExpiration
                     , CacheItemPriority.AboveNormal
                     , null);
                }

                return HttpContext.Current.Cache["Setting"] as Setting;
            }
        }

        public static void ClearCaches()
        {
            HttpContext.Current.Cache.Remove("Setting");
            HttpContext.Current.Cache.Remove("Pages");
            HttpContext.Current.Cache.Remove("EditorPick");
            HttpContext.Current.Cache.Remove("Categories");
        }
    }
}