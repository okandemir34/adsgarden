﻿const user = (function () {

    const variables = {
        "element": {
            "api_base": app.services.api.base,
            "userId": localStorage.getItem("UserId"),
            "channelId": localStorage.getItem("ChannelId"),
            "interval": null,
            "loader": $(".loader"),
            ajax_settings: {
                type: "GET"
            },
        }
    };

    function init() {
    }

    return {
        init: init,
    };

}());