﻿const main = (function () {

    const variables = {
        "element": {
            "api_base": app.services.api.base,
            "sesionReload": "/Home/SessionReload",
            "addNewTask": "/TodoList/AddNewTask",
            "getEditTask":"/TodoList/TaskEdit?id=",
            "postEditTask": "/TodoList/TaskEdit",
            "deleteTask": "/TodoList/TaskDelete?id=",
            "changeType": "/TodoList/ChangeType?id=",
            "uploadImage": "/TodoList/UploadPhoto",
            "todoDetail": "/TodoList/TodoDetail?id=",
            "dailyMission": "/Home/GetSideRequest",
            "pomodoroFinish": "/Home/PomdoroFinish?id=",
            "pomodoroStart": "/Home/PomdoroStart?id=",
            "pomodoroDelete": "/Home/PomdoroDelete?id=",
            "taskId": 0,
            "get_Session":"Home/SessionGetherer",
            "status":0,
            "app_token": localStorage.getItem("UserGuid"),
            "interval": null,
            "timer":null,
            "timerInterval": null,
            "pomodoroId": 0,
            "loader": $(".loader"),
            ajax_settings: {
                type: "GET"
            },
        }
    };
    function bindEvents() {
        $("body").on("click", "[data-role='addNewTask']", OpenModal);
        $("body").on("click", "#sendForm", addNewTask);
        $("body").on("click", ".taskEdit", getTaskEdit);
        $("body").on("click", ".taskDelete", taskDelete);
        $("body").on("click", ".addPhoto", openPhotoEditor);
        $("body").on("click", "[data-role='changeType']", changeType);
        $("body").on("click", "[data-role='aproved']", pomodoroStart);
        $("body").on("click", "[data-role='reject']", pomodoroDelete);
        $("body").on("click", ".todoDetail", todoDetail);
        console.log("main.js loaded");
    }

    function SessionReload() {
        variables.element.ajax_settings.url = variables.element.sesionReload;
        variables.element.ajax_settings.type = "GET";
        app.ajaxHelper(variables.element.ajax_settings, function (data) {
            console.log(data);
            if (data === false) { clearInterval(interval);}
        });
    }

    function GetSideRequests() {
        console.log("istek yaptı");
        variables.element.ajax_settings.url = variables.element.dailyMission;
        variables.element.ajax_settings.type = "GET";
        app.ajaxHelper(variables.element.ajax_settings, function (data) {
            if (data.length > 0) $(".side-card").html(SideMisssions(data));
            else $(".side-card").html(SideNoMission);
        });
    }

    function SideNoMission() {
        let html = "";
        html += '<div class="card-body">'+
                    '<p>Tüm Pomodoroları Bitirmişsin Yeni Eklemek İster Misin Sahip?</p>'+
                    '<a href="/DailyMission/Add" class="btn btn-success w-100">Yeni Ekle</a>'+
                '</div>';
        return html;
    }

    function Cooldown(finish, id) {
        
        finish = finish.replace('"', "");
        var m = finish.split(",")[0];
        var d = finish.split(",")[1];
        var y = finish.split(",")[2].split(" ")[0];
        var h = finish.split(" ")[1].split(":")[0];
        var min = finish.split(" ")[1].split(":")[1];
        var s = finish.split(" ")[1].split(":")[2].replace('"',"");
        var date = new Date();
        // zamanı set edelim burada
        date.setDate(d);
        //js de tarihler +1 ilerliyor ondan -1 yazdım
        date.setMonth(m - 1);
        date.setFullYear(y);
        date.setHours(h);
        date.setMinutes(min);
        date.setSeconds(s);
        var countDownDate = date.getTime();
        
        // Şuan ki zamanı alalım ki bitiş zamanından çıkartıcam
        var now = new Date().getTime();
        
        // şimdi bitiş zamanı ile şuan ki zamanı çıkartıp değişkene atayalım
        var distance = countDownDate - now;
        
        // burada gün, saat, dakika, ve saniye cinsinden bırakıyorum belki sen kullanırsın 
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        
        // konsola yazdırıyorum süreyi sonra bunu sidebara eklicem 
        /*var time = days + "d " + hours + "h "
            + minutes + "m " + seconds + "s ";*/

        var time = hours + " Saat "
            + minutes + " Dakika " + seconds + " Saniye Kaldı";

        if (distance < 0) {
            clearInterval(variables.element.timerInterval);
            $("#timer").html("<p class='badge p-2 w-100 badge-success'>Pomodoro Bitti</p>");
            pomodoroFinish(variables.element.pomodoroId);
            return false;
        }

        $("#timer").html("<p class='badge p-2 w-100 badge-danger'>" + time +"</p>");
    }

    function pomodoroFinish(id) {
        console.log(id);
        variables.element.ajax_settings.url = variables.element.pomodoroFinish + id;
        variables.element.ajax_settings.type = "GET";
        app.ajaxHelper(variables.element.ajax_settings, function (data) {
            console.log(data);
        });
    }

    function pomodoroStart() {
        var id = $(this).attr("data-id");
        var status = $(this).attr("data-status");
        variables.element.ajax_settings.url = variables.element.pomodoroStart + id + "&status=" + status;
        variables.element.ajax_settings.type = "GET";
        app.ajaxHelper(variables.element.ajax_settings, function (data) {
            if (data) {
                new PNotify({
                    title: 'Pomdoro Başladı',
                    text: 'Pomodoro başladı hadi odaklan.',
                    addclass: 'bg-success border-success'
                });
                GetSideRequests();
            }
        });
    }

    function pomodoroDelete() {
        
        var id = $(this).attr("data-id");
        var notice = new PNotify({
            title: 'Dikkat!',
            text: '<p>Bu pomodoroyu silmek üzeresiniz. Emin misiniz?</p>',
            hide: false,
            type: 'warning',
            confirm: {
                confirm: true,
                buttons: [
                    {
                        text: 'Yes',
                        addClass: 'btn btn-sm btn-primary'
                    },
                    {
                        addClass: 'btn btn-sm btn-link'
                    }
                ]
            },
            buttons: {
                closer: false,
                sticker: false
            }
        });

        notice.get().on('pnotify.confirm', function () {
            var status = $(this).attr("data-status");
            variables.element.ajax_settings.url = variables.element.pomodoroDelete + id + "&status=0";
            variables.element.ajax_settings.type = "GET";
            app.ajaxHelper(variables.element.ajax_settings, function (data) {
                if (data) {
                    new PNotify({
                        title: 'Başarılı',
                        text: 'Pomodoro silme işlemi tamamlandı.',
                        icon: 'icon-warning22',
                        type: 'success'
                    });
                    GetSideRequests();
                }
                else {
                    new PNotify({
                        title: 'Hata Oldu',
                        text: 'Pomodoro silme esnasında bir problem oldu anlamadım.',
                        icon: 'icon-warning22',
                        type: 'danger'
                    });
                }
            });
        });

        notice.get().on('pnotify.cancel', function () {
            new PNotify({
                title: 'İptal Edildi.',
                text: 'Pomodoro silme işlemini iptal ettin kankam.',
                icon: 'icon-blocked',
                type: 'warning'
            });
        });
        
        
    }

    function SideMisssions(data) {
        var html = "";
        var color = "danger";
        var icon = "icon-alarm";
        for (k in data) {
            var item = data[k];
            var active = item.IsActive;
            console.log(active);
            html += "<div class='card-body px-2 py-2'>" +
                        "<div class='media-list'>" +
                            "<div class='media'>" +
                                "<div class='mr-3 text-center'>" +
                                    "<span class='btn bg-" + color + " text-white rounded-round border-0 btn-icon'><i class=" + icon + "></i></span>" +
                                    "<p class='font-weight-semibold mb-0 text-" + color + "'>" + item.Duration + "</p>" +
                                "</div>"+
                            "<div class='media-body'>" +
                                "<div class='form-group mb-0' id='timer'>";
                                    if (active === true) {
                                        console.log(variables.element.pomodoroId);
                                        variables.element.pomodoroId = item.Id;
                                        variables.element.timerInterval = setInterval(function () {
                                            Cooldown(item.EndDateString, variables.element.pomodoroId);
                                        }, 1000);
                                        html += "<p class='badge p-2 w-100 badge-success'>Süre hesaplanıyor</p>";
                                    }
                                    else {
                                        html +=     '<div class="w-100 d-flex"><a href="javascript:void(0);">' +
                                                        '<i class="icon-checkmark3 btn btn-success btn-sm float-left mr-2" data-role="aproved" data-id="' + item.Id + '" data-status="' + 1 + '"></i>' +
                                                    '</a>' +
                                                    '<a href="javascript:void(0);">' +
                                                        '<i class="icon-cross2 btn btn-danger btn-sm float-left mr-2" data-role="reject" data-id="' + item.Id + '" data-status="' + 0 + '"></i>' +
                                                    '</a></div>';
                                    }
            html +=             "</div>" +
                                "<p class='font-size-sm mb-0'>" +
                                    "Başlık : " + item.Title + "" +
                                "</p>" +
                                "<p class='font-size-sm mb-0'>" +
                                    "Özet : " + item.Summary + "" +
                                "</p>" +
                            "</div>"+
                        "</div>" +
                    "</div>" +
                "</div>";
        }

        return html;
    }

    function OpenModal() {
        variables.element.status = 1;
        $("#todoListTypeId").val("");
        $("#todoTitle").val("");
        $("#todoSummary").val("");
        $("#todoDescription").val("");
        $("#todoTableId").val("");
        $('#modal_form_vertical').modal('toggle');
    }

    function getTaskEdit() {
        variables.element.status = 2;
        variables.element.taskId = $(this).attr("data-id");
        variables.element.ajax_settings.url = variables.element.getEditTask + variables.element.taskId;
        variables.element.ajax_settings.type = "GET";
        app.ajaxHelper(variables.element.ajax_settings, function (data) {
            console.log(data);
            $("#projectId").val(data.TodoProjectId);
            $("#todoTitle").val(data.Title);
            $("#todoSummary").val(data.Summary);
            $("#todoDescription").val(data.Description);
            $("#todoTableId").val(data.TodoProjectId);
            $("#todoListTypeId").val(data.TodoListTypeId);
            $('#modal_form_vertical').modal('toggle');
            $("#modal_form_vertical > #sendForm").addClass("test");
        });
    }

    function postTaskEdit() {
        variables.element.ajax_settings.url = variables.element.postEditTask;
        var postData = {};
        postData.TodoProjectId = $("#projectId").val();
        postData.Title = $("#todoTitle").val();
        postData.Summary = $("#todoSummary").val();
        postData.Description = $("#todoDescription").val();
        postData.TodoTableId = $("#todoTableId").val();
        postData.TodoListTypeId = $("#todoListTypeId").val();
        postData.Id = variables.element.taskId;
        variables.element.ajax_settings.data = postData;
        variables.element.ajax_settings.type = "POST";
        app.ajaxHelper(variables.element.ajax_settings, function (data) {
            if (data) location.reload();
            else alert("hata oldu");
        });
    }

    function addNewTask() {
        if (variables.element.status === 2) { postTaskEdit(); }
        else {
            variables.element.ajax_settings.url = variables.element.addNewTask;
            var postData = {};
            postData.TodoProjectId = $("#projectId").val();
            postData.Title = $("#todoTitle").val();
            postData.Summary = $("#todoSummary").val();
            postData.Description = $("#todoDescription").val();
            postData.TodoTableId = $("#todoTableId").val();
            postData.TodoListTypeId = $("#todoListTypeId").val();
            variables.element.ajax_settings.data = postData;
            variables.element.ajax_settings.type = "POST";
            app.ajaxHelper(variables.element.ajax_settings, function (data) {
                if (data) location.reload();
                else alert("hata oldu");
            });
        }
    }

    function taskDelete() {
        // Setup
        variables.element.taskId = $(this).attr("data-id");
        var row = $("[data-col-id='" + variables.element.taskId + "']");
        var notice = new PNotify({
            title: 'Emin misin?',
            text: '<p>Bu görevi silmek üzeresin bundan emin misin?</p>',
            hide: false,
            type: 'warning',
            confirm: {
                confirm: true,
                buttons: [
                    {
                        text: 'Yes',
                        addClass: 'btn btn-sm btn-primary'
                    },
                    {
                        addClass: 'btn btn-sm btn-link'
                    }
                ]
            },
            buttons: {
                closer: false,
                sticker: false
            }
        });

        notice.get().on('pnotify.confirm', function () {
            variables.element.ajax_settings.url = variables.element.deleteTask + variables.element.taskId;
            variables.element.ajax_settings.type = "GET";
            app.ajaxHelper(variables.element.ajax_settings, function (data) {
                if (data) {
                    new PNotify({
                        title: 'Görev Sil',
                        text: 'Görev silme işlemi tamamlandı.',
                        icon: 'icon-warning22',
                        type: 'success'
                    });
                    row.html("");
                }
                else {
                    new PNotify({
                        title: 'Görev Sil',
                        text: 'Görevi silme esnasında bir problem oldu anlamadım.',
                        icon: 'icon-warning22',
                        type: 'danger'
                    });
                }
            });
        });

        notice.get().on('pnotify.cancel', function () {
            new PNotify({
                title: 'Görev Sil',
                text: 'Görev silme işlemini iptal ettin kankam.',
                icon: 'icon-blocked',
                type: 'warning'
            });
        });
    }

    function changeType() {
        let typeId = $(this).attr("type-id");
        let dataId = $(this).attr("data-id");
        variables.element.ajax_settings.url = variables.element.changeType + typeId + "&dataid=" + dataId;
        variables.element.ajax_settings.type = "GET";
        app.ajaxHelper(variables.element.ajax_settings, function (data) {
            if (data) location.reload();
            else alert("hata oldu");
        });
    }

    function addPhoto(dataId,projectId,image) {
        variables.element.ajax_settings.url = variables.element.uploadImage;
        var postData = {};
        postData.ProjectId = projectId;
        postData.TodoListId = dataId;
        postData.ImagePath = image;
        variables.element.ajax_settings.data = postData;
        variables.element.ajax_settings.type = "POST";
        app.ajaxHelper(variables.element.ajax_settings, function (data) {
            if (data) location.reload();
            else alert("hata oldu");
        });
    }

    function openPhotoEditor() {
        let projectId, dataId;
        projectId = $(this).attr("data-project");
        dataId = $(this).attr("data-id");
        var avatar = document.getElementById('avatar');
        var image = document.getElementById('image');
        var input = document.getElementById('input');
        var $modal = $('#modal');
        var cropper;

        input.addEventListener('change', function (e) {
            var files = e.target.files;
            var done = function (url) {
                input.value = '';
                image.src = url;
                $modal.modal('show');
            };
            var reader;
            var file;
            var url;

            if (files && files.length > 0) {
                file = files[0];

                if (URL) {
                    done(URL.createObjectURL(file));
                } else if (FileReader) {
                    reader = new FileReader();
                    reader.onload = function (e) {
                        done(reader.result);
                    };
                    reader.readAsDataURL(file);
                }
            }
        });

        $modal.on('shown.bs.modal', function () {
            cropper = new Cropper(image, {
                dragMode: 'move',
                aspectRatio: 16 / 9,
                autoCropArea: 0.65,
                restore: false,
                guides: false,
                center: false,
                highlight: false,
                cropBoxMovable: false,
                cropBoxResizable: false,
                toggleDragModeOnDblclick: false,
                minContainerWidth: 600,
                minContainerHeight: 355,
                minCanvasWidth: 600,
                minCanvasHeight: 355,
                minCropBoxWidth: 600,
                minCropBoxHeight: 355,
                zoomable: true,
            });
        }).on('hidden.bs.modal', function () {
            cropper.destroy();
            cropper = null;
        });

        document.getElementById('crop').addEventListener('click', function () {
            var canvas;
            $modal.modal('hide');

            if (cropper) {
                canvas = cropper.getCroppedCanvas({
                    width: 600,
                    height: 355,
                });
                initialAvatarURL = avatar.src;
                avatar.src = canvas.toDataURL();
                canvas.toBlob(function (blob) {
                    var avat = $("#avatar").attr("src");
                    $("#imageBaseCode").val(avat);
                    console.log(avat);
                    addPhoto(dataId, projectId, avat);
                });
            }
        });
    }

    function todoDetail() {
        var id = $(this).attr("data-id");
        variables.element.ajax_settings.url = variables.element.todoDetail + id;
        variables.element.ajax_settings.type = "GET";
        app.ajaxHelper(variables.element.ajax_settings, function (data) {
            console.log(data);
            $('#modal_todo_detail').modal('toggle');
            $(".todoDetailTitle").html(data.TodoDetail.Title);
            $(".totoDetailSummary").html(data.TodoDetail.Summary);
            $(".totoDetailDescription").html(data.TodoDetail.Description);
            if (data.TodoPhoto.length > 0) {
                var html = "";
                for (k in data.TodoPhoto) {
                    var item = data.TodoPhoto[k];
                    html += "<a href='" + item.ImagePath + "' target='_blank'><img src='" + item.ImagePath + "'  width='150' height='150' class='rounded mr-2 img-thumbnail float-left' /></a>";
                }
                $(".todoDetailImages").html(html);
            }
        });
    }

    function init() {
        bindEvents();
        GetSideRequests();
        //interval = setInterval(function () { GetSideRequests(); }, 30000);
    }

    return {
        init: init,
    };

}());