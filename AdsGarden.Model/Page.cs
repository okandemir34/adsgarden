﻿namespace AdsGarden.Model
{
    using System;

    public class Page : Core.ModelBase
    {
        public string Title { get; set; }
        public string Slug { get; set; }
        public string Description { get; set; }
        public int SiteId { get; set; }
        public bool IsActive { get; set; }
    }
}
