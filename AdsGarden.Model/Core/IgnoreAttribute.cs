﻿using System;

namespace AdsGarden.Model.Core
{
    public class IgnoreAttribute : Attribute
    {
        public string SomeProperty { get; set; }
    }
}
