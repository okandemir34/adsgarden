﻿namespace AdsGarden.Model
{
    using System;

    public class Setting : Core.ModelBase
    {
        public string Sitename { get; set; }
        public string LogoPath { get; set; }
        public string Ads300 { get; set; }
        public string Ads600 { get; set; }
        public string Analytics { get; set; }
        public string AdsenseVerifiedCode { get; set; }
        public string SiteAdress { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Youtube { get; set; }
        public string Instagram { get; set; }
        public string Pinterest { get; set; }
        public bool IsSslDomian { get; set; }
    }
}
