﻿namespace AdsGarden.Model
{
    using System;

    public class Category : Core.ModelBase
    {
        public string Title { get; set; }
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string Keywords { get; set; }
        public string Description { get; set; }
        public string Slug { get; set; }
        public DateTime CreateDate { get; set; }
        public int SiteId { get; set; }
        public bool IsActive { get; set; }
    }
}
