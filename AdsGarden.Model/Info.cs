﻿namespace AdsGarden.Model
{
    using System;

    public class Info : Core.ModelBase
    {
        public string Desinger { get; set; }
        public string Owner { get; set; }
        public string GardenStyle { get; set; }
        public string Size { get; set; }
        public string Climate { get; set; }
        public string Location { get; set; }
        public int ContentId { get; set; }
    }
}
